# Fake Speed Test
with this code you can create fake speed test in speedtest.net

#Run
for run, go to FakeSpeedTest Folder and enter Command:
```sh
$ perl speedtest.pl -d 200 -u 100 -p 1
```
#Help
if after run see this error
`Can't locate Geo/Distance.pm in @INC (you may need to install the Geo::Distance module)`
install geo perl module
```sh
$ sudo apt-get install libgeo-distance-perl
``` 
